package com.bruno;

public class Main {

	static Menu mainMenu = new Menu();

	public static void main(String[] args) throws InterruptedException {

		String loc = System.getenv("USERPROFILE");
		System.out.println("Default location: " + loc);

		mainMenu.start(loc);

		Thread.sleep(3000);
	}
}
