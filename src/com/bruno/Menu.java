package com.bruno;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Menu {

	JFrame mainWindow = new JFrame("Fileburner 5k");

	public void start(String loc){

		prepareMenu(loc);
	}

	public void dummy() {
		System.out.println("test1");
	}

	public void prepareMenu(String loc) {

		EventQueue.invokeLater(() -> {
			JPanel textpanel = new JPanel();
			textpanel.add(new JLabel("Welcome to Fileburner 5000, let me be your guide", SwingConstants.CENTER));

			JPanel buttonsPanel = new JPanel();
			JButton button1 = new JButton("Creation");
			JButton button2 = new JButton("Copying");
			JButton button3 = new JButton("Removal");
			JButton button4 = new JButton("Read");

			button1.addActionListener(e -> create(loc));

			button2.addActionListener(e -> copy());

			button3.addActionListener(e -> remove(loc));

			button4.addActionListener(e -> read());

			buttonsPanel.add(button1);
			buttonsPanel.add(button2);
			buttonsPanel.add(button3);
			buttonsPanel.add(button4);

			JSplitPane splitter = new JSplitPane(JSplitPane.VERTICAL_SPLIT);

			splitter.setTopComponent(textpanel);
			splitter.setBottomComponent(buttonsPanel);
			splitter.setDividerLocation(30);

			mainWindow.add(splitter);
			mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			mainWindow.pack();
			mainWindow.setLocationRelativeTo(null);
			mainWindow.setVisible(true);

		});

	}

	private void create(String loc){
		String filename = loc + "//testfile.txt";
		File file = new File(filename);
		try {
			if(!(file.exists())) {
				file.createNewFile();
				System.out.println("file created in location " + loc);
				Desktop.getDesktop().open(new File(loc));
			}
			else
			{
				System.out.println("file already exists");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void copy() {
		dummy();
	}

	private void remove(String loc) {

		String filename = loc + "//testfile.txt";
		File file = new File(filename);
		try {
			if (file.exists()) {
				file.delete();
				System.out.println("file deleted from: " + loc);
			} else {
				System.out.println("file doesn't exist");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void read() {
		dummy();
	}

	public void welcome() {


		System.out.println("Welcome to FileExploder101, I will be your guide");
		System.out.println("1. Creation");
		System.out.println("2. Copying");
		System.out.println("3. Removal");
		System.out.println("4. Read");

		Scanner input = new Scanner(System.in);
		int s = 0;

		while (true) {
			try {
				s = input.nextInt();
			} catch (InputMismatchException exc) {
				input.next();
				System.out.println(exc.toString() + ": wrong value error");
			}

			switch (s) {
				case 1:
					System.out.println("chosen: Creation");
					dummy();
					break;
				case 2:
					System.out.println("chosen: Copying");
					dummy();
					break;
				case 3:
					System.out.println("chosen: Removal");
					dummy();
					break;
				case 4:
					System.out.println("chosen: Read");
					dummy();
					break;
				default:
					System.out.println("no go, choose a different one");
					break;
			}
		}

	}


}
